//
//  UIViewExtension.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit

extension UIView {

    /// Adds list of subviews.
    ///
    /// - Parameter subviews: Subviews to add to the view.
    func add(subviews: UIView...) {
        subviews.forEach {
            addSubview($0)
        }
    }
}
