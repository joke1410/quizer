//
//  Errors.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

enum ObjectMappingError: Error {
    case failedToMap(object: String)
    case failedToMapList(ofObjects: String)
}
