//
//  ManagedQuizStatus+CoreDataProperties.swift
//  
//
//  Created by Peter Bruz on 05/12/2016.
//
//

import Foundation
import CoreData


extension ManagedQuizStatus {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedQuizStatus> {
        return NSFetchRequest<ManagedQuizStatus>(entityName: "ManagedQuizStatus");
    }

    @NSManaged public var mngd_quizIdentifier: String
    @NSManaged public var mngd_lastQuestionOrder: NSNumber
    @NSManaged public var mngd_percentageProgress: String?
    @NSManaged public var mngd_lastResult: String?
    @NSManaged public var mngd_lastPercentageResult: String?

}
