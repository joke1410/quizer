//
//  QuizStatus.swift
//  Quizer
//
//  Created by Peter Bruz on 05/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

struct QuizStatus {
    
    /// Identifier of quiz that the status is related to.
    let quizId: String

    /// Last question order. Contains `0` if does not apply.
    var lastQuestionOrder: Int

    /// String value of progress in quiz (with `%` sign attached, e.g. `10%`).
    var percentageProgress: String?

    /// String value of last result. Examples of form of data: `1/8`, `3/5`, `11/12`.
    var lastResult: String?

    /// String value of last result in percents (with `%` sign attached, e.g. `10%`).
    var lastPercentageResult: String?
}
