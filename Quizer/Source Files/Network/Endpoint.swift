//
//  Endpoint.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

protocol Endpoint {

    /// Scheme of endpoint.
    var scheme: String { get }

    /// Host of endpoint.
    var host: String { get }

    /// Version on endpoint.
    var version: String { get }
}


/// Represents endpoint of quiz API.
struct QuizApiEndpoint: Endpoint {
    var scheme = "https"
    var host = "quiz.o2.pl"
    var version = "/api/v1"
}
