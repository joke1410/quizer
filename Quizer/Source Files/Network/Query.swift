//
//  Query.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

/// Method used in HTTP request.
///
/// - GET: GET request.
/// - POST: POST request.
/// - PUT: PUT request.
/// - DELETE: DELETE request.
enum Method: String {
    case GET, POST, PUT, DELETE
}

protocol Query {

    /// Method to use with query.
    var method: Method { get }

    /// Endpoint to use with query.
    var endpoint: Endpoint { get }

    /// Path to use with query.
    var path: String { get }
}

extension Query {

    var endpoint: Endpoint {
        return QuizApiEndpoint()
    }
}
