//
//  Request.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation
import SwiftyJSON

enum RequestError: Error {
    case failedToBuildRequest
}

enum ResponseError: Error {
    case badResponse
    case notFound
}

struct Request {

    /// Query to use while performing request.
    var query: Query

    /// Session to use while performing request.
    var session: URLSession {
        return URLSession.shared
    }

    private var finalRequest: URLRequest? {

        var urlComponents = URLComponents()
        urlComponents.scheme = query.endpoint.scheme
        urlComponents.host = query.endpoint.host
        urlComponents.path = query.endpoint.version + query.path

        guard let url = urlComponents.url else { return nil }

        var request = URLRequest(url: url)
        request.httpMethod = query.method.rawValue

        return request
    }

    /// Invokes request.
    ///
    /// - Parameter completion: Closure that is called when performing background request finishes.
    func invoke(completion: @escaping (ResultType<JSON?>) -> Void) {

        guard let finalRequest = finalRequest else {
            completion(ResultType.failure(RequestError.failedToBuildRequest))
            return
        }
        session.dataTask(with: finalRequest) { data, response, error in

            if let error = error {
                completion(ResultType.failure(error))

            } else {

                guard let httpResponse = response as? HTTPURLResponse else {
                    completion(ResultType.failure(ResponseError.badResponse))
                    return
                }

                switch httpResponse.statusCode {
                case 200..<207: completion(ResultType.success(JSON(data: data ?? Data())))
                case 404: completion(ResultType.failure(ResponseError.notFound))
                default: completion(ResultType.failure(ResponseError.badResponse))
                }
            }
        }.resume()
    }
}
