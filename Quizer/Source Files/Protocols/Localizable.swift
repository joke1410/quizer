//
//  Localizable.swift
//  Quizer
//
//  Created by Peter Bruz on 06/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

/// Localizable object
protocol Localizable {

    /// Provides localized string
    var localized: String { get }
}

extension Localizable where Self: RawRepresentable, Self.RawValue == String {

    var localized: String {
        return .localized(withKey: self.rawValue)
    }
}
