//
//  ManagedQuizStatusProvider.swift
//  Quizer
//
//  Created by Peter Bruz on 05/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import CoreData

final class ManagedQuizStatusProvider {

    fileprivate var managedObjectContext: NSManagedObjectContext {
        return (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    }

    /// Provides managed quiz status from Core Data based on QuizStatus structure.
    ///
    /// - Parameter quizStatus: QuizStatus structure to base on.
    /// - Returns: Managed quiz status.
    func managedQuizStatus(quizStatus: QuizStatus) -> ManagedQuizStatus {
        let fetchRequest: NSFetchRequest<ManagedQuizStatus> = ManagedQuizStatus.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "mngd_quizIdentifier == %@", quizStatus.quizId)

        if let firstFetchedObject = try? managedObjectContext.fetch(fetchRequest).first,
            let managedQuizStatus = firstFetchedObject {
            return managedQuizStatus
        }

        let managedQuizStatusEntity = NSEntityDescription.entity(forEntityName: ManagedQuizStatus.entityName, in: managedObjectContext)!
        let managedQuizStatus = ManagedQuizStatus(entity: managedQuizStatusEntity, insertInto: managedObjectContext)
        managedQuizStatus.mngd_quizIdentifier = quizStatus.quizId
        managedQuizStatus.mngd_lastQuestionOrder = NSNumber(value: Int32(quizStatus.lastQuestionOrder))
        managedQuizStatus.mngd_percentageProgress = quizStatus.percentageProgress
        managedQuizStatus.mngd_lastResult = quizStatus.lastResult
        managedQuizStatus.mngd_lastPercentageResult = quizStatus.lastPercentageResult

        return managedQuizStatus
    }

    /// Provides list of managed quizzes' statuses.
    ///
    /// - Returns: List of managed quizzes' statuses.
    /// - Throws: Error if providing fails.
    func managedQuizStatusList() throws -> [ManagedQuizStatus] {
        let fetchRequest: NSFetchRequest<ManagedQuizStatus> = ManagedQuizStatus.fetchRequest()

        do {
            let managedQuizStatusList = try managedObjectContext.fetch(fetchRequest)

            return managedQuizStatusList

        } catch let error {
            throw error
        }
    }
}
