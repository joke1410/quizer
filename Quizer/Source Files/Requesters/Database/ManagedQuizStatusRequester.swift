//
//  ManagedQuizStatusRequester.swift
//  Quizer
//
//  Created by Peter Bruz on 05/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import CoreData

final class ManagedQuizStatusRequester {

    fileprivate var managedObjectContext: NSManagedObjectContext {
        return (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    }

    fileprivate lazy var managedQuizStatusProvider = ManagedQuizStatusProvider()

    /// Saves quiz status to Core Data.
    ///
    /// - Parameter quizStatus: Quiz status to save
    /// - Throws: Error while saving.
    func save(quizStatus: QuizStatus) throws {
        let managedQuizStatus = managedQuizStatusProvider.managedQuizStatus(quizStatus: quizStatus)
        managedQuizStatus.mngd_lastQuestionOrder = NSNumber(value: Int32(quizStatus.lastQuestionOrder))
        managedQuizStatus.mngd_percentageProgress = quizStatus.percentageProgress
        managedQuizStatus.mngd_lastResult = quizStatus.lastResult
        managedQuizStatus.mngd_lastPercentageResult = quizStatus.lastPercentageResult

        do {
            try managedObjectContext.save()
        } catch let error {
            throw error
        }
    }
}
