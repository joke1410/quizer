//
//  QuizRequester.swift
//  Quizer
//
//  Created by Peter Bruz on 05/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation


/// Use this class to make operations related to quiz (other than `GET`).
final class QuizRequester {

    fileprivate lazy var databaseQuizStatusRequester = DatabaseQuizStatusRequester()

    /// Saves quiz status.
    ///
    /// - Parameters:
    ///   - quizStatus: Quiz status to save.
    ///   - completion: Completion that is called after saving quiz status.
    ///                 If saving succeeds, ResultType is `success`, otherwise it's `failed` and contains error.
    func save(quizStatus: QuizStatus, completion: (ResultType<Void>) -> Void) {
        databaseQuizStatusRequester.save(quizStatus: quizStatus, completion: completion)
    }
}
