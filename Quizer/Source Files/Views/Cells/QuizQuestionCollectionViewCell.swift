//
//  QuizQuestionCollectionViewCell.swift
//  Quizer
//
//  Created by Peter Bruz on 04/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import EasyPeasy

import UIKit
import EasyPeasy
import OAStackView

class QuizQuestionCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = String(describing: QuizQuestionCollectionViewCell.self)

    let questionLabel = UILabel()

    var didSelectAnswerWithOrder: ((Int) -> Void)?

    var imageUrl: URL? = nil {
        didSet {
            imageView <- [
                Height(0).when { self.imageUrl == nil },
                Height(150).when { self.imageUrl != nil }
            ]
            activityIndicator.startAnimating()
            imageView.sd_setImage(with: imageUrl) { [weak self] _ in
                self?.activityIndicator.stopAnimating()
            }
        }
    }

    var answers: [(order: Int, text: String)] = [] {
        didSet {
            answers.forEach {
                let answerView = AnswerView()
                answerView.answerTextLabel.text = $0.text
                let order = $0.order
                answerView.didTap = { [weak self] in
                    self?.disableSelectingAnswers()
                    self?.didSelectAnswerWithOrder?(order)
                }
                answersStackView.addArrangedSubview(answerView)
            }
        }
    }

    fileprivate let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    fileprivate let imageView = UIImageView()
    fileprivate let answersStackView = OAStackView()
    fileprivate let answersScrollView = UIScrollView()
    fileprivate let centeredContentView = UIView()
    fileprivate let questionContentView = UIView()

    // MARK: Life cycle

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
        setupHierarchy()
        setupInitialConstraints()
    }

    @available(*, unavailable, message : "Use init(frame:) method instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        let answerViews = answersStackView.arrangedSubviews
        answerViews.forEach {
            answersStackView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }

        imageView.sd_cancelCurrentImageLoad()
        imageView.image = nil
        activityIndicator.stopAnimating()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        answersScrollView.contentSize = answersStackView.frame.size
    }
}

// MARK: Private

private extension QuizQuestionCollectionViewCell {

    func setupViews() {
        contentView.backgroundColor = .clear

        centeredContentView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        centeredContentView.layer.cornerRadius = 3
        centeredContentView.layer.borderColor = UIColor.appDarkBlue.withAlphaComponent(0.6).cgColor
        centeredContentView.layer.borderWidth = 1

        questionContentView.backgroundColor = UIColor.appDarkBlue.withAlphaComponent(0.6)

        questionLabel.textColor = .white
        questionLabel.textAlignment = .center
        questionLabel.numberOfLines = 0

        imageView.contentMode = .scaleAspectFit

        answersStackView.alignment = .fill
        answersStackView.axis = .vertical
        answersStackView.distribution = .equalSpacing
        answersStackView.spacing = 6
        answersStackView.translatesAutoresizingMaskIntoConstraints = false

        answersScrollView.showsVerticalScrollIndicator = false
    }

    func setupHierarchy() {
        contentView.add(subviews: centeredContentView)
        imageView.add(subviews: activityIndicator)
        questionContentView.add(subviews: questionLabel, imageView)
        centeredContentView.add(subviews: questionContentView, answersScrollView)
        answersScrollView.add(subviews: answersStackView)
    }

    func setupInitialConstraints() {

        centeredContentView <- Edges(20)

        questionContentView <- [
            Leading(), Trailing(),
            Top()
        ]

        questionLabel <- [
            Leading(20), Trailing(20),
            Top(20)
        ]

        imageView <- [
            Leading(20), Trailing(20),
            Top(10).to(questionLabel, .bottom), Height(0), Bottom(10)
        ]

        answersScrollView <- [
            Leading(20), Trailing(20),
            Top(20).to(imageView, .bottom), Bottom(5)
        ]

        answersStackView <- [
            Leading(), Trailing(),
            Top(), Width().like(answersScrollView)
        ]

        activityIndicator <- Center()
    }

    func disableSelectingAnswers() {
        answersStackView.arrangedSubviews.forEach {
            ($0 as? AnswerView)?.selectable = false
        }
    }
}
