//
//  QuizTableViewCell_iPad.swift
//  Quizer
//
//  Created by Peter Bruz on 06/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import EasyPeasy

final class QuizTableViewCell_iPad: UITableViewCell, QuizTableViewCellProtocol {

    fileprivate enum StaticText: String, Localizable {
        case lastResult = "QuizTableViewCell/last-result"
        case solveQuiz = "QuizTableViewCell/solve-quiz"
    }

    static let reuseIdentifier = String(describing: QuizTableViewCell_iPad.self)

    var lastResult: String? = nil {
        didSet {
            resultLabel.text = { if let result = lastResult {
                return "\(StaticText.lastResult.localized)\n\(result)"
                }
                return StaticText.solveQuiz.localized
            }()
        }
    }

    var progress: String? = nil {
        didSet {
            progressLabelContainerView.isHidden = progress == nil
            progressLabel.text = progress
        }
    }

    let titleLabel = UILabel()
    let backgroundImageView = UIImageView()

    fileprivate let resultLabel = UILabel()
    fileprivate let progressLabel = UILabel()
    fileprivate let titleLabelContainerView = UIView()
    fileprivate let resultLabelContainerView = UIView()
    fileprivate let progressLabelContainerView = UIView()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupHierarchy()
        setupInitialConstraints()
    }

    @available(*, unavailable, message: "Use init(style:reuseIdentifier:) instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        backgroundImageView.sd_cancelCurrentImageLoad()
        backgroundImageView.image = nil
    }
}

private extension QuizTableViewCell_iPad {

    func setupViews() {

        selectionStyle = .none
        backgroundColor = .lightGray

        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.clipsToBounds = true

        titleLabelContainerView.backgroundColor = UIColor.appDarkBlue.withAlphaComponent(0.5)

        titleLabel.font = UIFont.systemFont(ofSize: 16)
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = .white

        resultLabelContainerView.backgroundColor = UIColor.appDarkBlue.withAlphaComponent(0.7)
        resultLabelContainerView.layer.cornerRadius = 3
        resultLabel.textColor = .white
        resultLabel.numberOfLines = 0
        resultLabel.textAlignment = .center

        progressLabelContainerView.backgroundColor = UIColor.appDarkBlue.withAlphaComponent(0.7)
        progressLabelContainerView.layer.cornerRadius = 3
        progressLabel.textAlignment = .right
        progressLabel.textColor = .white
    }

    func setupHierarchy() {
        titleLabelContainerView.add(subviews: titleLabel)
        resultLabelContainerView.add(subviews: resultLabel)
        progressLabelContainerView.add(subviews: progressLabel)
        contentView.add(subviews: backgroundImageView, titleLabelContainerView, resultLabelContainerView, progressLabelContainerView)
    }

    func setupInitialConstraints() {

        contentView <- Edges()

        backgroundImageView <- [
            Width(400), Trailing(),
            Top(), Bottom(1)
        ]


        titleLabelContainerView <- [
            Leading(), Right().to(backgroundImageView, .left),
            Top(), Bottom(1)
        ]

        titleLabel <- Edges(10)

        resultLabelContainerView <- [
            Trailing(3), Top(3)
        ]
        
        resultLabel <- Edges(5)
        
        progressLabelContainerView <- [
            Trailing(3), Bottom(3)
        ]
        
        progressLabel <- Edges(5)
    }
}
