//
//  QuizzesView.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import EasyPeasy

final class QuizzesView: UIView {

    let tableView = UITableView(frame: .zero, style: .plain)
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)

    // MARK: Life Cycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupHierarchy()
        setupInitialConstraints()
    }

    @available(*, unavailable, message: "Use init(frame:) instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: Private

private extension QuizzesView {

    func setupViews() {
        tableView.backgroundColor = .darkGray
        tableView.separatorStyle = .none

        activityIndicator.startAnimating()
    }

    func setupHierarchy() {
        add(subviews: tableView, activityIndicator)
    }

    func setupInitialConstraints() {

        tableView <- [
            Leading(), Trailing(),
            Top(20), Bottom()
        ]

        activityIndicator <- Center()
    }
}
