//
//  AnswerMapperTest.swift
//  Quizer
//
//  Created by Peter Bruz on 06/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import Quizer

class AnswerMapperTest: XCTestCase {

    var mapper = AnswerMapper()
    var expectedCorrectAnswer: Answer!
    var expectedIncorrectAnswerWithUrl: Answer!

    override func setUp() {
        super.setUp()

        expectedCorrectAnswer = correctAnswerMock
        expectedIncorrectAnswerWithUrl = incorrectAnswerMock
    }

    func testMapCorrectAnswer() {

        guard
            let filepath = Bundle(for: type(of: self)).path(forResource: "correctAnswer", ofType:"json"),
            let fileContent = try? String(contentsOfFile: filepath)
        else {
            XCTAssert(false)
            return
        }

        let json = JSON.parse(fileContent)


        guard let answer = try? mapper.mapToAnswer(json: json) else {
            XCTAssert(false)
            return
        }

        guard let expectedAnswer = expectedCorrectAnswer else {
            XCTAssert(false)
            return
        }

        XCTAssert(answer == expectedAnswer)
    }

    func testMapIncorrectAnswer() {

        guard
            let filepath = Bundle(for: type(of: self)).path(forResource: "incorrectAnswerWithUrl", ofType:"json"),
            let fileContent = try? String(contentsOfFile: filepath)
            else {
                XCTAssert(false)
                return
        }

        let json = JSON.parse(fileContent)


        guard let answer = try? mapper.mapToAnswer(json: json) else {
            XCTAssert(false)
            return
        }

        guard let expectedAnswer = expectedIncorrectAnswerWithUrl else {
            XCTAssert(false)
            return
        }
        
        XCTAssert(answer == expectedAnswer)
    }
}

private extension AnswerMapperTest {

    var correctAnswerMock: Answer {
        return Answer(text: "correctAnswer", order: 1, imageUrl: nil, isCorrect: true)
    }

    var incorrectAnswerMock: Answer {
        return Answer(text: "incorrectAnswer", order: 1, imageUrl: URL(string: "https://url.com"), isCorrect: false)
    }
}
