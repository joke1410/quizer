//
//  QuizQueryTest.swift
//  Quizer
//
//  Created by Peter Bruz on 06/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import XCTest
@testable import Quizer

class QuizQueryTest: XCTestCase {

    var quizQuery: QuizQuery!

    override func setUp() {
        super.setUp()

        quizQuery = QuizQuery(id: "12345")
    }

    func testQuizQuery() {

        guard let query = quizQuery else {
            XCTAssert(false)
            return
        }

        XCTAssert(query.method == Method.GET)

        XCTAssert(query.path == "/quiz/12345/0")
    }
}
