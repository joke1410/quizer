//
//  QuizzesQueryTest.swift
//  Quizer
//
//  Created by Peter Bruz on 06/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import XCTest
@testable import Quizer

class QuizzesQueryTest: XCTestCase {

    var quizzesQuery: QuizzesQuery!

    override func setUp() {
        super.setUp()

        quizzesQuery = QuizzesQuery()
    }

    func testQuizzesQuery() {

        guard let query = quizzesQuery else {
            XCTAssert(false)
            return
        }

        XCTAssert(query.method == Method.GET)

        XCTAssert(query.path == "/quizzes/0/100")
    }
}
