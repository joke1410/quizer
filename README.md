# Quizer

Welcome to the **Quizer** project. It's an application made for iOS for recruitment needs.

### Tools & requirements

- Xcode 8 with iOS 8.0+ SDK

Would be nice to have:

- [CocoaPods](https://github.com/CocoaPods/CocoaPods) 1.1.1

### Configuration

Assuming the above tools are already installed, run the following commands after cloning the repository:

- `pod install`

ATTENTION!
If you don't want to install pods on your own, use `with-pods` branch.

### Coding guidelines

- Respecting community [Swift style guide](https://github.com/github/swift-style-guide).
- There shouldn't be any commented-out code.

### Requirements of task vs. prepared solution

#### Must have

All done.

#### Nice to have

I had not enough time to cover it fully, so here is what I did: 

Dedicated layout for iPad:

- I made one example how to proceed with this issue. You can review it in `QuizTableViewCell`. The clue is to create protocol that 2 different views conform to. If it's done properly, splitting desings/layout between iPhone and iPad should not be a big problem.

Tests:

- I wrote just a couple of Unit tests.

Lost/recovery of network connection:

- Nope, unfortunatelly it's not in the solution. It turned out that time is finite...
